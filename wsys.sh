#!/bin/sh

TTL=$(ping -c 1 $1 | grep -Po 'ttl=\d+' | awk -F = '{ print $2 }')


if [[ $TTL -le 64 ]]; then
  echo -e "[*] ttl="$TTL" -> OS Linux"
  echo -e "[*] There are $(expr 64 - $TTL) nodes between your machine and the objective -> $1"

elif [[ $TTL -le 128 ]] && [[ $TTL -gt 64 ]]; then
  echo -e "[*] ttl="$TTL" -> OS Windows"
  echo -e "[*] There are $(expr 128 - $TTL) nodes between your machine and the objective -> $1"

else
  echo "[*] OS cannot be detected."
fi

